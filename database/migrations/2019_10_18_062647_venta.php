<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Venta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // crear tabla venta
        Schema::create('venta', function (Blueprint $table){
           $table -> increments('id');
           $table -> integer('cliente_id');
           $table -> string('cliente_nombre');
           $table -> double('total',9,2);
           $table -> date('fecha');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // dropear la tabla para el roll
        Schema::dropIfExists('venta');
    }
}
