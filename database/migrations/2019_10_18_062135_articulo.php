<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Articulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // crear tabla articulo
        Schema::create('articulo', function (Blueprint $table){
            $table -> increments('id');
            $table -> string('descripcion');
            $table -> string('modelo');
            $table -> double('precio', 9, 2);
            $table -> integer('existencia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // dropear la tabla para el rollback
        Schema::dropIfExists('articulo');
    }
}
