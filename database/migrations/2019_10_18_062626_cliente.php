<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // crear tabla cliente
        Schema::create('cliente', function (Blueprint $table){
            $table -> increments('id');
            $table -> string('nombre');
            $table -> string('ape_pat');
            $table -> string('ape_mat');
            $table -> string('rfc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // dropear la tabla
        Schema::dropIfExists('cliente');
    }
}
