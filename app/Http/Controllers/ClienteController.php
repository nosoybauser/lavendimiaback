<?php

namespace App\Http\Controllers;

use App\cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get
        $data = cliente::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // post
        $data = $request->all();
        try {
            $cliente = cliente::create($data);
            return response()->json($cliente,200);
        } catch (\Exception $e) {
            return response()->json(['Error:'['no se pudo crear el cliente']],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get by id
        $cliente = cliente::findOrFail($id);
        try {
            return response()->json($cliente, 200);
        } catch (\Exception $e) {
            return response()->json(['Error: '['No se encontro el cliente']],422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // put
        $data = $request->all();
        try {
            $cliente = cliente::where('id',$id)->firstOrFail();
            $cliente->update($data);
            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json(['Error: '['No se actualizo el cliente']], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        try {
            return cliente::where('id',$id)->delete();
        } catch (\Exception $e) {
            return response()->json(['Error: '['no se pudo eliminar el cliente'],422]);
        }
    }
}
