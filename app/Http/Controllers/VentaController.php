<?php

namespace App\Http\Controllers;

use App\venta;
use Illuminate\Http\Request;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get
        $data = venta::all();
        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // post
        $data = $request->all();
        try {
            $venta = venta::create($data);
            return response()->json($venta,200);
        } catch (\Exception $e) {
            return response()->json(['Error:'['no se pudo crear la venta']],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         // get by id
         $venta = venta::findOrFail($id);
         try {
             return response()->json($venta, 200);
         } catch (\Exception $e) {
             return response()->json(['Error: '['No se encontro la venta']],422);
         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(venta $venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // put
        $data = $request->all();
        try {
            $venta = venta::where('id',$id)->firstOrFail();
            $venta->update($data);
            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json(['Error: '['No se actualizo la venta']], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        try {
            return venta::where('id',$id)->delete();
        } catch (\Exception $e) {
            return response()->json(['Error: '['no se pudo eliminar la venta'],422]);
        }
    }
}
