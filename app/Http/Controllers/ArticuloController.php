<?php

namespace App\Http\Controllers;

use App\articulo;
use Illuminate\Http\Request;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return articulo::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        try {
            $articulo = articulo::create($data);
            return response()->json($articulo,200);
        } catch (\Exception $e) {
            return response()->json(['Error: '['Error al crear el articulo']]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get by id

        try {
            return articulo::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['Error: '[$e]]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function edit(articulo $articulo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // put
        $data = $request->all();
        try {
            $articulo = articulo::where('id',$id)->firstOrFail();
            $articulo->update($data);
            return response()->json($articulo,200);
        } catch (\Exception $e) {
            return response()->json($data,422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        try {
            return articulo::where('id',$id)->delete();
        } catch (\Exception $e) {
            return response()->json(['Error: '['no se pudo eliminar el articulo'],422]);
        }
    }
}
