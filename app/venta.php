<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
    protected $table = 'venta';
    protected $fillable = ['cliente','articulos','total','abono','fecha'];
    public $timestamps = false;
}
