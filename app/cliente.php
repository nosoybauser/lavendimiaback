<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    //
    protected $table = 'cliente';
    protected $fillable = ['nombre','ape_pat','ape_mat','rfc'];
    public $timestamps = false;
}
